var app = angular.module('hello', ['ngRoute', 'ui.bootstrap.tpls', 'ui.bootstrap'])
        .config(function ($routeProvider, $httpProvider) {
            $routeProvider.when('/', {
                templateUrl: 'home.html',
                controller: 'home'
            }).when('/login', {
                templateUrl: 'views/login.html',
                controller: 'navigation'
            }).when('/login2', {
                templateUrl: 'views/login2.html',
                controller: 'login2'
            }).when('/signup1', {
                templateUrl: 'views/signup1.html',
                controller: 'signup1'
            }).when('/signup2', {
                templateUrl: 'views/signup2.html',
                controller: 'signup2'
            }).otherwise('/');

            $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';

        })
        .controller('home', function ($scope, $http) {
            $http.get('/resource/').success(function (data) {
                $scope.greeting = data;
            });
        })
        .controller('navigation',
                function ($rootScope, $scope, $http, $location) {
                    var authenticate = function (credentials, callback) {

                        var headers = credentials ? {authorization: "Basic "
                                    + btoa(credentials.username + ":" + credentials.password)
                        } : {};

                        $http.get('user', {headers: headers}).success(function (data) {
                            if (data.name) {
                                $rootScope.authenticated = true;
                                $http.get('/isCorrectAnswer').success(function (result) {
                                    if (result === "") {
                                        $location.path("/login2");
                                    }
                                });
                            } else {
                                $rootScope.authenticated = false;
                            }
                            callback && callback();
                        }).error(function () {
                            $rootScope.authenticated = false;
                            callback && callback();
                        });

                    };

                    $scope.credentials = {};

                    authenticate();

                    $scope.login = function () {
                        authenticate($scope.credentials, function () {
                            if ($rootScope.authenticated) {
                                $location.path("/login2");
                                $scope.error = false;
                            } else {
                                $location.path("/login");
                                $scope.error = true;
                            }
                        });
                    };

                    $scope.logout = function () {
                        $http.post('logout', {}).success(function () {
                            $rootScope.authenticated = false;
                            $location.path("/");
                        }).error(function (data) {
                            $rootScope.authenticated = false;
                        });
                    };
                });
app.controller('signup1', function ($rootScope, $scope, $http, $location) {
    $scope.submitSignup1 = function () {
        $http.post('/signup1', $scope.signup1).success(function () {
            $location.path("/signup2");
        }).error(function (data) {
            console.log("values error")
        });
    };

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };
    
    $http.get('/signup1').success(function(result) {
        if(result !== "") {
            $scope.signup1 = result;
        }
    });
});
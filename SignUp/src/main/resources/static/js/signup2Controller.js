app.controller('signup2', function ($rootScope, $scope, $http, $location) {
    $http.get('/signup1').success(function (result) {
        if (result === "") {
            $location.path("/signup1");
        }
    });

    $scope.submit = function () {
        $http.post('/signup2', $scope.signup2).success(function (result) {
            $location.path('/');
        });
    };
});
app.controller('login2', function ($rootScope, $scope, $http, $location) {
    if($rootScope.authenticated)
        $http.get('/isCorrectAnswer').success(function (result) {
            if (result === "") {
                $location.path("/login2");
            }
        });
    else
        $location.path('/');

    $scope.submit = function () {
        $http.post('/isCorrectAnswer', $scope.login2).success(function (result) {
            debugger;
            if (result === 'true') {
                $rootScope.isCorrectAnswer = true;
                $location.path('/');
            }
        });
    };
});
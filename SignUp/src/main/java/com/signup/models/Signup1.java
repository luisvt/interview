/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signup.models;

import java.util.Date;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author luis
 */
public class Signup1 {
    @NotEmpty public String username;
    @NotEmpty public String password;
    @NotNull  public Date birthday;
}

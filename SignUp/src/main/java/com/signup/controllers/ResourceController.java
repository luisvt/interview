/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signup.controllers;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author luis
 */
@RestController
public class ResourceController {
    @SuppressWarnings("serial")
	@RequestMapping("/resource/")
    public Map<String, Object> home() {
        return new HashMap<String, Object>(){{
            put("id", UUID.randomUUID().toString());
            put("content", "Hello World");
        }};
    }
}

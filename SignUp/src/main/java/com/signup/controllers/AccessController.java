/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signup.controllers;

import com.signup.models.Login2;
import com.signup.models.Signup1;
import com.signup.models.Signup2;
import com.signup.models.SysUser;
import com.signup.repositories.UserRepository;
import java.security.Principal;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author luis
 */
@RestController
public class AccessController {

    @Autowired
    UserRepository userRepository;

    @RequestMapping(value = "/signup1", method = RequestMethod.POST)
    public void saveSignup1ToSession(@Valid @RequestBody Signup1 signup1, HttpSession session) {
        session.setAttribute("signup1", signup1);
    }

    @RequestMapping("/signup1")
    public Signup1 getSignup1(HttpSession session) {
        return (Signup1) session.getAttribute("signup1");
    }

    @RequestMapping(value = "/signup2", method = RequestMethod.POST)
    public void signup2(@Valid @RequestBody Signup2 signup2, HttpSession session) {
        Signup1 signup1 = (Signup1) session.getAttribute("signup1");
        session.removeAttribute("signup1");
        userRepository.save(new SysUser(
                signup1.username,
                new BCryptPasswordEncoder().encode(signup1.password),
                signup1.birthday,
                signup2.question1,
                signup2.question2,
                signup2.answer1,
                signup2.answer2));
    }
    
    @RequestMapping(value = "/isCorrectAnswer", method = RequestMethod.POST)
    public boolean checAnswer(@RequestBody Login2 login2, Principal user, HttpSession session) {
        SysUser u = userRepository.findOneByUsername(user.getName());
        boolean isCorrectAnswer =  login2.question.equals(u.getQuestion1()) && login2.answer.equals(u.getAnswer1())
                || login2.question.equals(u.getQuestion2()) && login2.answer.equals(u.getAnswer2());
        session.setAttribute("isCorrectAnswer", isCorrectAnswer);
        return isCorrectAnswer;
    }
    
    @RequestMapping("/isCorrectAnswer")
    public Boolean isCorrectAnswer(HttpSession session) {
        return (Boolean) session.getAttribute("isCorrectAnswer");
    }

    @RequestMapping("/user")
    public Principal user(Principal user) {
        return user;
    }
}

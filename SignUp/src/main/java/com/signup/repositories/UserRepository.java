/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signup.repositories;

import org.springframework.data.repository.CrudRepository;
import com.signup.models.SysUser;

/**
 *
 * @author luis
 */
public interface UserRepository extends CrudRepository<SysUser, Integer>{
    SysUser findOneByUsername(String username);
}

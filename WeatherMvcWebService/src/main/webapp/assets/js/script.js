$(document).on('submit', '#zipForm', function (e) {
    e.preventDefault();
    var weatherDiv = $('#weather').html('').removeClass('loaded error').removeAttr('hidden');
    $.getJSON("/weather", {city: $('#city').val()}, function(result) {
        var weatherContent = "";
        $.each(result, function(key, value) {
            var keySpaced = key.replace( /([A-Z])/g, " $1" );
            var keyTitle = keySpaced.charAt(0).toUpperCase() + keySpaced.slice(1);
            if(key !== 'success' && key !== 'responseText' && key !== 'weatherID')
                weatherContent += "<tr><td><b>" + keyTitle + ":</td><td>" + value + "<td></tr>";
        });
        weatherDiv
                .removeClass('error')
                .html('<table><tbody>' + weatherContent + '</tbody></table>');
    }).fail(function(jqxhr, textStatus, error) {
        weatherDiv
                .addClass('error')
                .html(jqxhr.responseJSON.message);
    }).always(function () {
        weatherDiv.addClass('loaded');
    });
});

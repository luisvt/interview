/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.weathermvcwebservice.wsdl;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;
import org.weathermvcwebservice.wsdl.GetWeather;
import org.weathermvcwebservice.wsdl.GetWeatherResponse;

/**
 *
 * @author luis
 */
public class WeatherClient extends WebServiceGatewaySupport{
    public GetWeatherResponse getCityWeatherByZIPResponse(String cityName) {
        GetWeather request = new GetWeather();
        request.setCityName(cityName);
        
        return (GetWeatherResponse) getWebServiceTemplate().marshalSendAndReceive(
                request,
                new SoapActionCallback("http://www.webserviceX.NET/GetWeather"));
    }
}
